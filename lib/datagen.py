import requests, os, yaml, re, json, copy, collections, json
from datetime import datetime
from json.decoder import JSONDecodeError
from pprint import pprint
from lib import DATA_FULL_RESUME, DATA_SKILLS, RESUME_GEN_URL, SECURE_FILE, DATA_AWARDS, DATA_PROJECTS, DATA_PROFILES, DATA_WORK
from datetime import datetime, timedelta
from jinja2 import Environment, FileSystemLoader
from collections.abc import Mapping

theme_errors = {}

"""
This file is the primary logic for converting the data into the finished templates.  
"""


def merge_work(work_base, work_overrides):
    """ 
    This method will take the base work and merge it with the overrides.  This allows
    you to taylor various forms of a set of resumes
    """

    for ride in work_overrides:
        work = next(item for item in work_base if item["override_key"] == ride['override_key'])
        for key, value in ride.items():
            work[key] = value
    
    return work_base


def load_resume_data(data_file, years_back, include_secured, include_summary):
    """
    This will load the resume data from the various locations and prepare it to be applied to the
    template.  Any of the mutation parameters that are passed in the generate command, mutate the
    data here.

    :param years_back: Number of years back you want work history
    :param include_secured: flag to determine if you want to include the secured information or not.
    :param include_summary: flag to determine if the summary for work items and projects should be included in the data
    :return:
    """
    # load the full resume data file
    with open(data_file) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

    data['skills'] = load_skills()
    data['projects'] = load_include(DATA_PROJECTS, 'projects')
    data['awards'] = load_include(DATA_AWARDS, 'awards')
    data['basics']['profiles'] = load_include(DATA_PROFILES, 'profiles')

    # here we are loading the base work file, then applying the overrides
    # to have different variations of the work experience.
    work_base = load_include(DATA_WORK, 'work')
    work_overrides = copy.deepcopy(data['work'])

    data['work'] = merge_work(work_base, work_overrides)

    # this adds the configured projects to the base work
    for work in data['work']:
        work['base_work_skills'] = []
        for skill in data['skills']:
            for key in skill['keywords']:
                if work['override_key'] in skill['keywords'][key]:
                    work['base_work_skills'].append(key)
                    
    for project in data['projects']:
        project['project_skills'] = []
        for skill in data['skills']:
            for key in skill['keywords']:
                if project['override_key'] in skill['keywords'][key]:
                    project['project_skills'].append(key)

    # Load secured that I don't want in git
    if include_secured:
        with open(SECURE_FILE) as f:
            secdata = yaml.load(f, Loader=yaml.FullLoader)
            dict_merge(data, secdata)

    if years_back != 0:
        year = timedelta(days=365)
        years = years_back * year
        for d in data['work']:
            if 'endDate' in d:
                time_between_insertion = datetime.now() - datetime.strptime(d['endDate'], '%Y-%m-%d')
                if time_between_insertion.days > years.days:
                    d.pop("summary", None)
                    d.pop("highlights", None)

    if not include_summary:
        for d in data['work']:
            d.pop("summary", None)

        for d in data['projects']:
            d.pop("description", None)

    return data


def dict_merge(dct, merge_dct):
    """ Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.

    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :return: None
    """
    for k, v in merge_dct.items():
        if (k in dct and isinstance(dct[k], dict)
                and isinstance(merge_dct[k], Mapping)):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]


def load_skills():
    """
    I wasn't a fan of the schema for skills.  So I reimplemented the yaml file into a format I find
    easier to maintain.  This method loads that skills file and reformats it into the structure that
    is needed by the template system.

    """
    # Load the skills set and attach it.
    with open(DATA_SKILLS) as f:
        skills_data = yaml.load(f, Loader=yaml.FullLoader)

    skills = []
    
    for key, value in skills_data.items():
        for level_key, level_value in value.items():
            skill = {'name': key, 'level': level_key, 'keywords': level_value}
            skills.append(skill)

    return skills


def load_include(file_name, data_key):
    """
    This method is designed to load the data from an external file.  This way the same 
    data can be used to generate multiple variations of the same resume.

    """
    # Load the project set and attach it.
    with open(file_name) as f:
        project_data = yaml.load(f, Loader=yaml.FullLoader)

    return project_data[data_key]


def generate_resumes(years_back, include_secured, themes, include_summary, picture):
    """
    This method does the hard work of passing the data into the template system.

    In the future, I would like this to generate an index page as well, but I havn't found a good
    timeline system I like yet.

    :param years_back: Number of years back you want work history
    :param include_secured: flag to determine if you want to include the secured information or not.
    :param themes: Array of themes to be processed
    :param include_summary: flag to determine if the summary for work items and projects should be included in the data
    :param picture: flag to determine if the profile picture should be included or not.
    :return:
    """

    # generate resumes
    for dataset in DATA_FULL_RESUME:
        data = load_resume_data(dataset['datafile'], years_back, include_secured, include_summary)

        data['theme_picture'] = picture

        for theme in themes:
            output = render_theme(theme, data)
            if not os.path.exists("public"):
                os.mkdir("public")
                
            with open("public/" + dataset['outfile'], "w") as file:
                file.write(output)

            with open("public/" + dataset['jsonout'], "w") as outfile:
                outfile.write(json.dumps(data, indent=2))
    
    # generate_index(data, themes)


def render_theme(theme, data):
    """
    This takes the theme, initializes jinja2, and passes the data to the render system.

    In each theme is a theme.yaml file.  This file is also loaded and the data thats included
    in it is passed to jinja.

    :param theme: The name of the Theme to be rendered
    :param data: processed dataset to be rendered
    :return: The final HTML in a string format
    """
    print("Generating: " + theme)

    path_theme = os.path.join(os.getcwd(), "themes", theme)
    path_file_theme = os.path.join(path_theme, "theme.yaml")

    with open(path_file_theme) as f:
        theme_file = yaml.load(f, Loader=yaml.FullLoader)

    file_loader = FileSystemLoader(path_theme)
    env = Environment(loader=file_loader)
    env.filters['datetimeformat'] = datetimeformat
    
    template = env.get_template(theme_file["resume_template"])
    main_css = read_css(path_theme, theme_file["css"]["primary"])
    print_css = read_css(path_theme, theme_file["css"]["print"])
    
    return template.render(css=main_css, print=print_css, resume=data)


def generate_index(data, themes):
    """
    This is a work in progress method.  The goal here is to analyze the data, and generate an index
    page that has my career timeline.  Once I find a timeline system I like, I will finish this.

    :param data: processed dataset to be rendered
    :param themes: The name of the Theme to be rendered
    :return: The final HTML in a string format
    """
    data['themes'] = themes

    timelines = []

    year_min = datetime.now().year
    year_max = datetime.now().year

    for item in data['work']:
        item_list = [item['startDate']]

        d = datetime.strptime(item['startDate'], "%Y-%m-%d")
        if year_min > d.year:
            year_min = d.year

        if 'endDate' in item:
            item_list.append(item['endDate'])

            d = datetime.strptime(item['endDate'], "%Y-%m-%d")
            if year_max < d.year:
                year_max = d.year

        else:
            item_list.append(datetime.today().strftime('%Y-%m-%d'))
        
        item_list.append(item['company'] + ' - ' + item['position'])
        item_list.append('lorem')

        timelines.append(item_list)

    data["timeline_min"] = year_min
    data["timeline_max"] = year_max
    data["timeline_data"] = json.dumps(timelines)

    output = render_theme("index", data)
    file = open("public/index.html", "w")
    file.write(output)


def datetimeformat(value):
    """
    convenient way to go from string dates to formated dates consistently.

    :param value: String of the date from the yaml
    :return: string of the standard formatted version
    """
    d = datetime.strptime(value, "%Y-%m-%d")
    return d.strftime('%B %Y')


def read_css(theme_dir, css_file):
    """
    Loads the specified CSS file into a string and passes it back to be inserted into the template.

    :param theme_dir: directory that contains the theme in question
    :param css_file: filename of the css file to be read in
    :return: The contents of the css file in a string
    """
    full_file_path = os.path.join(theme_dir, css_file)

    data = ""

    if os.path.exists(full_file_path): 
        with open(os.path.join(theme_dir, css_file), 'r') as myfile:
            data = myfile.read().replace('\n', '')

    return data
