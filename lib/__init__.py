
RESUME_GEN_URL = 'http://localhost:3000/theme/'
# RESUME_GEN_URL = 'http://themes.jsonresume.org/theme/'
RESUME_THEME_URL = 'http://themes.jsonresume.org/themes.json'

PAGE_SIZE = 50
RESUME_THEME_SEARCH = "https://api.npms.io/v2/search?q=jsonresume-theme"

DATA_FULL_RESUME = [
    # {
    #     'datafile': 'data/WilliamSlepecki-architect.yaml', 
    #     'outfile': 'arch.html',
    #     'jsonout': 'WilliamSlepecki-architect.json'
    # },
    {
        'datafile': 'data/WilliamSlepecki-devops.yaml', 
        'outfile': 'index.html', 
        'jsonout': 'WilliamSlepecki-devops.json'
    }
]
DATA_AWARDS = 'data/awards.yaml'
DATA_SKILLS = 'data/skills.yaml'
DATA_PROJECTS = 'data/projects.yaml'
DATA_WORK = 'data/base_work.yaml'
DATA_PROFILES = 'data/profiles.yaml'
THEMES_CONTROL = 'data/themes.yaml'
SECURE_FILE = 'secured.yaml'
