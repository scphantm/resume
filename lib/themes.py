import requests, yaml, math
from lib import RESUME_THEME_SEARCH, PAGE_SIZE, THEMES_CONTROL


def generate_themes_list():
    r = requests.get(RESUME_THEME_SEARCH, params={'size': 1})
    page = r.json()
    initial_total = page["total"]
    total_pages = math.ceil(initial_total / PAGE_SIZE)
    count = 0

    theme_name_list = []

    themes_control = load_themes_control()

    for i in range(total_pages):

        params_var = {'size': PAGE_SIZE}
        if i != 0:
            params_var["from"] = PAGE_SIZE*i

        page = requests.get(RESUME_THEME_SEARCH,
                            params=params_var)
        data = page.json()

        for result in data['results']:
            count += 1
            tmp = convert_pkg_name_to_theme(result['package'])
            if tmp is not None and tmp not in themes_control['exclude']:
                theme_name_list.append(tmp)

    return theme_name_list


def load_themes_control():
    with open(THEMES_CONTROL) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

    return data


def load_standard_themes():
    themes_control = load_themes_control()
    return themes_control['standard']


def convert_pkg_name_to_theme(data):
    prefix = "jsonresume-theme-"

    if data['name'].startswith(prefix):
        return remove_prefix(data['name'], prefix)
    else:
        return None


def remove_prefix(text, prefix):
    return text[text.startswith(prefix) and len(prefix):]
