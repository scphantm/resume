#!/usr/bin/env python3

import os, click, sys

CONTEXT_SETTINGS = dict(auto_envvar_prefix='COMPILERESUME')


class Context(object):

    def __init__(self):
        self.verbose = False
        self.home = os.getcwd()

    def log(self, msg, *args):
        """Logs a message to stderr."""
        if args:
            msg %= args
        click.echo(msg, file=sys.stderr)

    def vlog(self, msg, *args):
        """Logs a message to stderr only if verbose is enabled."""
        if self.verbose:
            self.log(msg, *args)


class ComplexCLI(click.MultiCommand):
    """
    Complex CLI Class allows the plugin system to work.  It provides functions that list possible commands
    in the command folder and retrieves them when passed in by the cli engine
    """

    def list_commands(self, ctx):
        rv = []
        for filename in os.listdir(cmd_folder):
            if filename.endswith('.py') and filename.startswith('cmd_'):
                rv.append(filename[4:-3])
        rv.sort()
        return rv

    def get_command(self, ctx, name):
        try:
            if sys.version_info[0] == 2:
                name = name.encode('ascii', 'replace')
            class_name = 'commands.cmd_' + name
            mod = __import__(class_name, None, None, ['cli'])
        except ImportError as err:
            print("Import Error: {0}".format(err))
            return
        return mod.cli


pass_context = click.make_pass_decorator(Context, ensure=True)

cmd_folder = os.path.abspath(os.path.join(os.path.dirname(__file__), 'commands'))


@click.command(cls=ComplexCLI, context_settings=CONTEXT_SETTINGS)
@pass_context
def cli(ctx):
    """
    Willie's resume compiler

    Hello boss, lets get a job
    """


if __name__ == '__main__':
    cli()