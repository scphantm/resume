import click

from compile import pass_context
from lib.datagen import generate_resumes
from lib.themes import generate_themes_list, load_standard_themes


@click.command('generate', short_help='Uses the data to generate the full resume')
@click.option('--years', default=0, help='The number of years back you want work history.  Default is zero, recommended is 10 for printed')
@click.option('--include-summary/--no-include-summary', default=True, help='Includes the work and project summaries in the output.')
@click.option('--all-themes/--no-all-themes', default=False, help='Processes all themes available.  Otherwise it will only do the main themes in the config file')
@click.option('--picture/--no-picture', default=True, help='Include the picture or not in the output')
@click.option('--include-secured/--no-include-secured', default=False, help='Include the secured information in the output or not')
@pass_context
def cli(ctx, years, include_summary, all_themes, picture, include_secured):
    """
    The primary generate command.  This will control all aspects of physically generating the resume.

    :param ctx: Needed by Click to maintain the context across the program
    :param years: The number of years back you want work history.  Default is zero, recommended is 10 for printed
    :param include_summary: Includes the work and project summaries in the output.
    :param all_themes: Processes all themes available.  Otherwise it will only do the main themes in the config file
    :param picture: Include the picture or not in the output
    :param include_secured: Include the secured information in the output or not
    """
    if all_themes:
        themes = generate_themes_list()
    else:
        themes = load_standard_themes()

    generate_resumes(years, include_secured, themes, include_summary, picture)
