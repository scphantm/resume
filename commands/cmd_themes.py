import click

from compile import pass_context
from lib.themes import generate_themes_list
from pprint import pprint


@click.command('themes', short_help='Searches NPM to get list of themes')
@pass_context
def cli(ctx):
    """
    This command is designed to search NPM to retrieve the list of themes associated with jsonresume.org and
    return them into a list.  This list can then be cultivated and turned into a resume theme here.

    :param ctx: Used by Click to maintain the context of the command
    """
    themes = generate_themes_list()
    pprint(themes)
    print(len(themes))
