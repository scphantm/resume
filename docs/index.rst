.. resume documentation master file, created by
   sphinx-quickstart on Mon Dec 24 21:34:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to resume's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   cmd_generate
   cmd_themes
   lib_datagen
   lib_themes


Overview of how this application works
======================================

I knew that I wanted to compile my resume from a data file instead of building another static document in Word or something
like that.  I wanted this compilation ability because I wanted to be able to have a single data source, and then compile
different variations of that data source as needed.

This application started as a variation of the `Json Resume <https://jsonresume.org/>`_ project.  I liked the compilation
abilities of that project, but I wasn't keen on a few aspects of it.

1. It was originally written in JavaScript.  JavaScript is fine for browser based apps, and more recently, simple server
   based REST systems.  But NPM has always given me problems on CLI systems.  Its a very difficult package manager to deal
   with and there are a great many things to go wrong.  I wanted a more reliable system.

2. The schema for Json Resume was a bit dated.  I wanted some additional fields and data in the schema.  Adding those
   fields on my own typically broke the Json Resume compiler.

3. I wanted to build a system that could manipulate the data, include certain things in one version but exclude them
   in another.

4. I prefer to work directly in Yaml instead of Json.  Its just a simpler format to deal with by hand.

After some experimentation, I decided to rewrite the Json Resume system in Python, using Jinja2 as the template compiler.
This ended up being a very simple solution that combined with Gitlab's CI/CD system, allows me to update the resume and
automatically publish it at will.

Right now, this is designed as an app only for me.  But maybe someday I will break it apart and refactor things to allow
me to publish this as a full program for others.  For now, it does what I need it to do.

In the `/data` folder are the following data files:

* skills.yaml - This file is the file for my skills.  I reformatted the schema into a format that is a bit easier to manage.
   Another reason to make it a separate file Is I picture this file being far more volatile than the primary data file
   and making it its own file cleans up the commit history a bit.

* themes.yaml - This controls the theme system.  The name of the themes you can compile are listed here, also the list
   of excluded themes.  The excluded themes date back to the NPM roots of this system and is primarily there for me to
   keep notes on which Json Resume templates I liked.

* WilliamSlepeckiFull.yaml - This is the primary data file.  All text data of the resume is in here.

The basic logic is very simple.  The `compile.py generate` command parses the Yaml files, creates a fully compiled
data object, initializes jinja2, and passes the data object to the template.  The return value from that rendering is
saved to a file.

Secured Parameters
==================

Obviously there are some parameters you don't want published to a public web site.  Things like your phone number or
your email.  But if you want to create a printed version of your resume, you have to include those things.  For this
application, thats handled with the `secured.yaml` file.  Create this file in the root of the project and put these
parameters in it

.. code-block:: yaml

   basics:
     email: ""
     phone: ""
     location:
       postalCode: ""
       city: ""
       countryCode: ""
       region: ""

You can put any value you want in this block.  The logic will merge the two dictionaries at compile time.  Then adding
the parameter `--include-secured` to the CLI command and the system will look for the secured file and include the data.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
