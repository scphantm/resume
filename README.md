This project is my personal resume.  The way this works is in the `/data` folder are the core resume files.  These resume files contain all 
information going all the way back in history.  The Python script is then used to template the resume data into various configurations.

[The completed resume can be found here](https://scphantm.gitlab.io/resume/)

[The Sphinx documentation on how this compiler works can be found here](https://scphantm.gitlab.io/resume/sphinx/index.html)

You can see the file `.gitlab-ci.yml` for the various commands to initialize and compile this resume.  In addition

`./compile.py generate --help` will give you the various command parameters.
